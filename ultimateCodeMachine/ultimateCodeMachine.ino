#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = <"yourNetwork">;
const char* password = <"yourPassword">;

int input0 = 14;
int input1 = 12;

unsigned char * charTable = (unsigned char *)malloc(1);
int sizeTable = 0;

int ledPin[8] = {5, 4, 0, 2, 13, 15, 3, 1};
int ledPinConfirm = 16;

bool myByte[8];
int indexByte = 0;

int in0 = 0;
int in1 = 0;

bool twinkle = false;

bool buttonPressed = false;

void resetPins()
{
  for (int i = 0; i <= 7; i++)
  {
    digitalWrite(ledPin[i], LOW);
    myByte[i] = false;
  }
  digitalWrite(ledPinConfirm, LOW);
  indexByte = 0;
}

void verifUnpressedButton()
{
  if (in0 == LOW && in1 == LOW)
  {
    buttonPressed = false;
  }
}

unsigned char myByteToChar()
{
  unsigned char myChar = 0;
  for (int i = 0; i < 8; i++)
  {
    unsigned char temp = 0x00000001;
    if (myByte[i])
    {
      temp <<= (7 - i);
      myChar |= temp;
    }
  }
  return myChar;
}

void addByte()
{
  charTable[sizeTable++] = myByteToChar();
  unsigned char * buff = (unsigned char *) malloc(sizeTable);
  for (int i = 0 ; i < sizeTable; i++)
  {
    buff[i] = charTable[i];
  }
  free(charTable);
  charTable = buff;
  //I had a bug for the 4th realloc, so screw it
}

MDNSResponder mdns;

ESP8266WebServer server(80);

void handleRoot() {
  String message = "<body>Hello from ESP8266 ! <br/> Here are the bytes your created : ";
  for (int i = 0; i < sizeTable; i++)
  {
    message += "<br/>" ;
    message += (unsigned char)charTable[i];
  }
  message += "</body>";
  String meta = "<meta http-equiv='Refresh' content='3'/>";
  server.send(200, "text/html", meta + message);
}

void handleBytes()
{
  String message = "";
  for (int i = 0; i < sizeTable; i++)
  {
    message += " " ;
    message += (unsigned char)charTable[i];
  }
  server.send(200, "text/plain", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  Serial.println("someone is lost");
}

void setup(void) {
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  Serial.println("");
  //
  myByteToChar();
  pinMode(ledPinConfirm, OUTPUT);
  for (int i = 0; i <= 7; i++)
  {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(input0, INPUT);
  pinMode(input1, INPUT);
  resetPins();
  //
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/about", []() {
    server.send(200, "text/html", "<html><body>Check <a href = 'https://gitlab.com/u/psyked/projects'>this gitLab </a> for more : <a> <br/> Project base on <a href = 'http://linuxfr.org/news/internet-des-objets-l-esp8266-et-ma-porte-de-garage'>this one</a> </html></body>");
    Serial.println("someone in about");
  });

  server.on("/bytes", handleBytes);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
  in0 = digitalRead(input0);
  in1 = digitalRead(input1);
  if (buttonPressed)
  {
    verifUnpressedButton();
  }
  else
  {
    if (indexByte >= 8)
    {
      digitalWrite(ledPinConfirm, HIGH);
      if (in1 == HIGH && in0 == LOW)
      {
        buttonPressed = true;
        addByte();
        resetPins();
      }
      if (in0 == HIGH)
      {
        buttonPressed = true;
        resetPins();
      }
    }
    else
    {
      if (in0 == HIGH && in1 == LOW)
      {
        buttonPressed = true;
        myByte[7 - indexByte] = false;
        digitalWrite(ledPin[indexByte++], LOW);
        delay(100);
      }
      if (in1 == HIGH)
      {
        buttonPressed = true;
        myByte[7 - indexByte] = true;
        digitalWrite(ledPin[indexByte++], HIGH);
        delay(100);
      }
    }
  }
  if (indexByte < 8)
  {
    twinkle = !twinkle;
    if (twinkle)
    {
      digitalWrite(ledPin[indexByte], HIGH);
    }
    else
    {
      digitalWrite(ledPin[indexByte], LOW);
    }
  }
  delay(100);
}
